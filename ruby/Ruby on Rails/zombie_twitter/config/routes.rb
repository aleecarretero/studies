ZombieTwitter < Application.routes.draw do
	resources :tweets
	root to: "tweets#index"
	get '/new_tweet' => 'tweets#new'
	get '/all' => 'tweets#index', as 'all_tweets'
	get '/all' => redirect('/tweets')
	get '/local_tweets/:zipcode' => 'tweets#index', as 'local_tweets'
	get ':name' => 'tweets#index', as 'zombie_tweets '
end