#!/usr/local/rvm/rubies/ruby-2.3.0/bin/ruby

def factorial(n)
    i = 1
    c = 1
    if n == 0
        return 1
    end
    while c != n
        i *= (c+1)
        c += 1
    end
    return i
end

def reverse(s)
    s = s.clone
    s.split(' ')
    half_size = s.size/2
    half_size.times {|i| s[i], s[-i-1] = s[-i-1], s[i]}
    return s
end

# def reverse(s)
#     return s.reverse
# end

def sort(a)
    i = 1
    while i < a.size
        c = i
        while c > 0
            if a[c] < a [c-1]
                a[c], a[c-1] = a[c-1], a[c]
            end
            c -= 1
        end
        i += 1
    end
    return a
end

def upcase(s)
    hash = { "é" => "É" "a" => "A", "b" => "B", "c" => "C", "d" => "D", "e" => "E", "f" => "F", "g" => "G", "h" => "H", "i" => "I", "j" => "J", "k" => "K", "l" => "L", "m" => "M", "n" => "N", "o" => "O", "p" => "P", "q" => "Q", "r" => "R", "s" => "S", "t" => "T", "u" => "U", "v" => "V", "w" => "W", "x" => "X", "y" => "Y", "z" => "Z"}
    for i in 0..(s.size-1)
        if hash.has_key?(s[i])
            s[i] = hash[s[i]]
        end
    end
    return s
end