var myButton = document.querySelector('button');
var myHeading = document.querySelector('h1');
function setUserName() {
    var myName = prompt('Please enter your name.');
    localStorage.setItem('name', myName);
    if (myName !== '') {
        myHeading.textContent = 'Oh! Hello, ' + myName + '...';
    } else {
        myHeading.textContent = 'Oh! Hello...';
    }
}
if (!localStorage.getItem('')) {
    setUserName();
} else {
    var storedName = localStorage.getItem('name');
    myHeading.textContent = 'Oh! Hello, ' + storedName + '...';
}
myButton.onclick = function() {
    setUserName();
}
