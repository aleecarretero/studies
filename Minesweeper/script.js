var timeout;
var gameStart = false;
var board = document.getElementById("boardDiv");
var boardSize = 0;
var numberOfBombs = 0;
var numberOfFlagsWithBombs = 0;
var totalBombs = 0;
var display = document.getElementById("infoDiv");
var matrix = null ;
var bombsCoordinates = [];
var bombPercent = 25;
var startButton = document.getElementById("startButton");
var userBoardSize = document.getElementById("userBoardSize");
var userBombPercent = document.getElementById("userBombPercent");
var loadBoard = function() {
    // 	removing existing board
    board.innerHTML = "";
    display.style.lineHeight = "50px";
    display.style.color = "black";
    numberOfFlagsWithBombs = 0;
    // create grid
    boardSize = document.getElementById("userBoardSize").value;
    totalBombs = parseInt((boardSize * boardSize) * (userBombPercent.value / 100));
    numberOfBombs = totalBombs;
    var sizePercent = 100 / boardSize;
    display.innerText = numberOfBombs + " bombs";
    var allCoordinates = [];
    matrix = new Array(boardSize);
    for (var y = 0; y < boardSize; y++) {
        //creates the lines
        var rowDiv = document.createElement("div");
        matrix[y] = new Array(boardSize);
        for (var x = 0; x < boardSize; x++) {
            //creates the rows
            var button = document.createElement("button");
            button.style.width = sizePercent + "%";
            button.style.height = sizePercent + "%";
            button.innerHTML = "&nbsp;";
            button.style.fontSize = (14 * 20) / boardSize + "px";
            rowDiv.appendChild(button);
            matrix[y][x] = button;
            button.x = x;
            button.y = y;
            allCoordinates.push(button);
            button.onclick = (e)=>{
              leftClickAction(e);
            }
            button.oncontextmenu = (e)=>{
              rightClickAction(e);
            }
          }
          board.appendChild(rowDiv);
        }
        generateBombs(allCoordinates);
        setLoading(false);
      }
      var generateBombs = function(allCoordinates) {
        bombsCoordinates = [];
        var count = 0;
        while (count++ < totalBombs) {
          var idx = Math.floor(Math.random() * allCoordinates.length);
          var coordinate = allCoordinates[idx];
          matrix[coordinate.y][coordinate.x].bomb = true;
          allCoordinates.splice(idx, 1);
          bombsCoordinates.push(coordinate);
        }
      }
      var showBombs = function() {
        for (var idx in bombsCoordinates) {
          var x = bombsCoordinates[idx].x;
          var y = bombsCoordinates[idx].y;
          if (matrix[y][x].markedAsFlag != true) {
            matrix[y][x].style.backgroundImage = "url('images/bombdeath.gif')";
            matrix[y][x].style.backgroundSize = "100%";
            matrix[y][x].style.backgroundPosition = "center";
            matrix[y][x].style.backgroundColor = "red";
            matrix[y][x].style.backgroundRepeat = "no-repeat";
          }
        }
      }
      var showFlag = function(element) {
        if (numberOfBombs == 0 && totalBombs != numberOfFlagsWithBombs) {
          setWarning("All flags used...");
          return
        }
        element.markedAsFlag = true;
        numberOfBombs--;
        if (element.bomb) {
          numberOfFlagsWithBombs++;
        }
        if (numberOfFlagsWithBombs == totalBombs) {
          gameOver(true);
        } else {
          display.innerText = numberOfBombs + " bombs";
        }
        element.style.backgroundImage = "url('images/bombflagged.gif')";
        element.style.backgroundSize = "100%";
        element.style.backgroundPosition = "center";
        element.style.backgroundRepeat = "no-repeat";
      }
      var hideFlag = function(element) {
        element.markedAsFlag = null ;
        numberOfBombs++;
        if (element.bomb) {
          numberOfFlagsWithBombs--;
        }
        display.innerText = numberOfBombs + " bombs";
        element.style.backgroundImage = "";
      }
      var leftClickAction = function(event) {
        if (gameStart == true) {
          var target = event.target;
          var isHumanClick = event.isTrusted;
          if (target.nodeName == "BUTTON" && target.markedAsFlag == true) {
            if (isHumanClick) {
              setWarning("Flags can't be opened");
              return;
            } else {
              if (!target.bomb) {
                hideFlag(target);
              } else {
                return;
              }
            }
          }
          hitPosition(target, isHumanClick, arguments[1]);
        }
      }
      var rightClickAction = function(event) {
        event.preventDefault();
        if (gameStart == true) {
          var target = event.target;
          if (target.nodeName == "BUTTON" && target.markedAsFlag == null ) {
            showFlag(target);
          } else {
            hideFlag(target);
          }
        }
      }
      var colors = ["red", "teal", "maroon", "black", "purple", "red", "green", "blue"];
      var hitPosition = function(button, isHumanClick) {
        button.onclick = null ;
        button.oncontextmenu = (e)=>{
          e.preventDefault();
        }

    // if (button.bomb)
    gameOver(false);
  } else {
    var neighborBombs = howManyNeighborBombs(button.x, button.y);
    button.neighborBombs = neighborBombs;
    if (neighborBombs != 0) {
      if (!button.bomb){
        button.innerText = neighborBombs;
        button.style.color = colors[colors.length - neighborBombs];
        var dontExplode = arguments[2];
        if (!dontExplode) {
          explode(button.x, button.y);
        }
      }
    } else {
      var dontExplode = arguments[2];
      if (!dontExplode) {
        explode(button.x, button.y);
      }
    }
    button.style.background = "#efefef";
  }
}
var explode = function(x, y) {
  var scheduledPositions = [];
  var pos = new Object();
  pos.x = x;
  pos.y = y;
  scheduledPositions = scheduledPositions.concat(gatherNeighborPositions(pos));
  while (scheduledPositions.length > 0) {
    var currentPosition = scheduledPositions.pop();
    var ignoreExplode = true;
    var event = new Object();
    event.target = currentPosition;
    if (!currentPosition.bomb){
      leftClickAction(event, ignoreExplode)
    }
    if (currentPosition.neighborBombs == 0) {
      var newPositions = gatherNeighborPositions(currentPosition);
      scheduledPositions = scheduledPositions.concat(newPositions);
    }
  }
}
var gatherNeighborPositions = function(pos) {
  var result = [];
  var x = pos.x;
  var y = pos.y;
  var iy = y == 0 ? y : y - 1;
  for (; iy <= y + 1 && iy < matrix.length; iy++) {
    var ix = x == 0 ? x : x - 1;
    for (; ix <= x + 1 && ix < matrix[iy].length; ix++) {
      if (matrix[iy][ix].onclick == null )
        continue;result.push(matrix[iy][ix]);
    }
  }
  return result;
}
var gameOver = function(win) {
  if (win) {
    display.innerText = "You Win! | Press Start";
    display.style.color = "green";
  } else {
    display.innerText = "Game Over | Press Start";
    display.style.color = "red";
    showBombs();
  }
  gameStart = false;
}
var howManyNeighborBombs = function(x, y) {
  var count = 0;
  var iy = y == 0 ? y : y - 1;
  for (; iy <= y + 1 && iy < matrix.length; iy++) {
    var ix = x == 0 ? x : x - 1;
    for (; ix <= x + 1 && ix < matrix[iy].length; ix++) {
      if (matrix[iy][ix].bomb) {
        count++;
      }
    }
  }
  return count;
}
var funcChange = function() {
  var userBoardSize = document.getElementById("userBoardSize");
  var userBombPercent = document.getElementById("userBombPercent");
  if (userBoardSize.value > 30 || userBombPercent.value > 70 || userBoardSize.value < 5 || userBombPercent.value < 10) {
    setWarning("Size has to be between 5 and 30 and percentage of \
      bombs has to be between 10% and 70%");
    userBoardSize.value = 20;
    userBombPercent.value = 15;
  }
  display.style.color = "black";
  display.style.lineHeight = "1.3em";
  display.innerHTML = "Click \"Start\" to restart with a " + userBoardSize.value + "x" + userBoardSize.value + " grid and " + parseInt((userBoardSize.value * userBoardSize.value) * (userBombPercent.value / 100)) + " bombs";
}
var setWarning = function(text) {
  var warning = document.getElementById("warning");
  clearTimeout(timeout);
  warning.style.visibility = "visible";
  warning.style.opacity = "1";
  warning.innerText = text;
  timeout = window.setTimeout(function() {
    warning.style.visibility = "hidden";
    warning.style.opacity = "0";
  }, 5000, text);
}
var setLoading = function(visible) {
  var loading = document.getElementById("loaderBg");
  if (visible == true){
    loading.style.visibility = "visible";
    loading.style.opacity = "1";
  }else{
    timeout = window.setTimeout(function(){
      loading.style.visibility = "hidden";
      loading.style.opacity = "0";
    },200)

  }
}
//preload images
function preloadImages(srcs) {
  if (!preloadImages.cache) {
    preloadImages.cache = [];
  }
  var img;
  for (var i = 0; i < srcs.length; i++) {
    img = new Image();
    img.src = srcs[i];
    preloadImages.cache.push(img);
  }
}
preloadImages(["images/bombdeath.gif", "images/bombflagged.gif", "images/mine.png"]);
// events
startButton.onclick = function() {
  setLoading(true);
  timeout = window.setTimeout(function(){
    var ubs = document.getElementById("userBoardSize");
    var ubp = document.getElementById("userBombPercent");
    if (ubs.value > 30 || ubp.value > 70 || ubs.value < 5 || ubp.value < 10) {
      setWarning("Size has to be between 5 and 30 and percentage of \
        bombs has to be between 10% and 70%");
      ubs.value = 20;
      ubp.value = 15;
    }

    loadBoard();
    gameStart = true;
  },500)
}
userBoardSize.onchange = function() {
  funcChange();
}
userBoardSize.onclick = function() {
  funcChange();
}
userBombPercent.onchange = function() {
  funcChange();
}
userBombPercent.onclick = function() {
  funcChange();
}
startButton.click();
